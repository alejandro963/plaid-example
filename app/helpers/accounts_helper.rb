module AccountsHelper
  def transactions_by_account(transactions)
    @transactions_by_account ||= transactions.group_by(&:account_id)
  end

  def numbers(num)
    num.map { |k, v| "#{k}: #{v}" }.join(" - ")
  end

  def location_for(location, sep: ', ')
    result = [
      location['address'], location['city'], location['state'], location['zip']
    ].compact.join(sep)
    if location['coordinates']
      result <<  " - Lat: #{location['coordinates']['lat']}"
      result << "#{sep}Long: #{location['coordinates']['lon']}"
    end
    result
  end

  def to_csv(user, transactions)
    CSV.generate do |csv|
      csv << [
        'Account ID', 'Current Balance', 'Available Balance', 'Institution',
        'Account Name', 'Account Number', 'Account Type', 'Account Subtype',
        'ID', 'Account ID',  'Date', 'Name', 'Amount', 'Location',
        'Category hirerarchy', 'Category ID', 'Meta',  'Pending',
        'Pending Transaction ID',  'Score',  'Type Primary', 'raw account',
        'raw transaction'
      ]
      user.accounts.each do |acc|
        transactions_by_account(transactions)[acc.id].each do |t|
          csv << [
            acc.id, acc.current_balance, acc.available_balance, acc.institution,
            acc.meta['name'], acc.meta['number'], acc.type, acc.subtype,
            t.id, t.account_id, t.date, t.name, t.amount,
            location_for(t.location, sep: '. '),
            t.category_hierarchy.to_a.join(' - '), t.category_id, t.meta,
            t.pending, t. pending_transaction_id, t.score, t.type[:primary],
            acc.to_json, t.to_json
          ]
        end
      end

    end
  end
end
