require 'csv'

class AccountsController < ApplicationController
  include AccountsHelper

  def index
    # Exchange the Link public_token for a Plaid API access token
    exchange_token_response = Plaid::User.exchange_token(params[:public_token])

    # Initialize a Plaid user
    auth_user = Plaid::User.load(:auth, exchange_token_response.access_token)

    @user = auth_user.for_product(:connect)

    @transactions = @user.transactions

    respond_to do |format|
      format.html
      format.csv do
        response.headers['Content-Type'] = 'text/csv'
        response.headers['Content-Disposition'] = 'attachment; filename=accounts.csv'
        send_data to_csv @user, @transactions
      end
    end
  end
end
